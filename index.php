<?php
    if (!isset($_SESSION)) {
        session_start();
    }

    require_once 'vendor/autoload.php';

    use App\Table;
    use App\Number;
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
        <title>Bingo</title>
        <style type="text/css">
            /*table {
                font-size: 25px;
            }

            td {
                padding: 0px !important;
            }*/
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <?php
                        if (isset($_POST['sort'])) {
                            echo '<h1 class="text-success text-center">Número Sorteado: '.Number::addNumber().'</h1>';
                        }

                        if (isset($_POST['clear'])) {
                            Number::reset();
                        }
                    ?>

                    <div class="text-center">
                        <form id="bingo" method="post" action="">
                            <button id="sort" name="sort" class="btn btn-primary" onclick="disable()">Sortear Número</button>
                            <button id="clear" name="clear" class="btn btn-danger" onclick="disable()">Limpar</button>
                        </form>
                    </div>
                </div>

                <div class="col-md-6">
                    <table class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th class="text-center">B</th>
                                <th class="text-center">I</th>
                                <th class="text-center">N</th>
                                <th class="text-center">G</th>
                                <th class="text-center">O</th>
                            </tr>
                        </thead>

                        <tbody>
                            <?= Table::mountTable(); ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <footer>
            <br/>
        </footer>

        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
        <script type="text/javascript">
            function disable() {
                $('button').addClass('disabled');
            }
        </script>
    </body>
</html>
