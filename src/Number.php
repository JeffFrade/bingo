<?php

namespace App;

class Number
{
    public static function addNumber()
    {
        $num = null;

        if (count($_SESSION['numbers']) < 75) {
            do {
                $num = rand(1, 75);
            } while (in_array($num, $_SESSION['numbers']));

            $_SESSION['numbers'][] = $num;
        }

        return $num;
    }

    public static function reset()
    {
        $_SESSION['numbers'] = [];
    }
}
