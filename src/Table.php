<?php

namespace App;

class Table
{
    public static function mountTable()
    {
        if (!isset($_SESSION['numbers'])) {
            $_SESSION['numbers'] = [];
        }

        $table = '';

        for ($i = 1; $i <= 15; $i++) {
            $table .= '<tr>';
            $table .= '<td class="text-center '.(in_array($i, $_SESSION['numbers'])?'bg-success text-light':'').'">' .$i .'</td>';
            $table .= '<td class="text-center '.(in_array($i + 15 * 1, $_SESSION['numbers'])?'bg-success text-light':'').'">' .($i  + 15 * 1).'</td>';
            $table .= '<td class="text-center '.(in_array($i + 15 * 2, $_SESSION['numbers'])?'bg-success text-light':'').'">' .($i  + 15 * 2).'</td>';
            $table .= '<td class="text-center '.(in_array($i + 15 * 3, $_SESSION['numbers'])?'bg-success text-light':'').'">' .($i  + 15 * 3).'</td>';
            $table .= '<td class="text-center '.(in_array($i + 15 * 4, $_SESSION['numbers'])?'bg-success text-light':'').'">' .($i  + 15 * 4).'</td>';
            $table .= '</tr>';
        }

        return $table;
    }
}